
use cursive::{CbSink, Cursive};
use cursive::views::{Button, Dialog, LinearLayout, SelectView, TextView, DummyView};
use cursive::traits::*;
use std::{thread, time::{Duration, Instant}};
use std::sync::mpsc::{Receiver, Sender, RecvError};

#[derive(Default)]
struct Data
{
    pub plot: bool,
    pub clear_data: bool,
    pub serial: String,
}

pub enum UiRequest {
    SetData(String, String),
}

pub enum UiResponse {
    Serial(String),
    Plot,
    SinglePlot,
    Averages(usize),
    Medians(usize),
    Window(usize),
    Clear,
    Sink(CbSink),
}

pub struct UserInterface {
    tx: Sender<UiRequest>,
    rx: Receiver<UiResponse>,
    sink: CbSink,
    serial: String,
    pub plot: bool,
    pub clear: bool,
    pub single_plot: bool,
    pub median_width: usize,
    pub averages_width: usize,
    pub window: usize
}

impl UserInterface {
    pub fn new(row_names: &'static [&str], column_names: &'static [&str]) -> Result<Self, RecvError> {
        let (instance_tx, interface_rx) = std::sync::mpsc::channel();
        let (interface_tx, instance_rx) = std::sync::mpsc::channel();
        thread::Builder::new().name("User Interface".to_owned()).spawn(move || {
            let mut runner = {
                let backend = cursive::backends::curses::n::Backend::init().unwrap();
                Cursive::default().into_runner(backend)
            };
            instance_tx.send(UiResponse::Sink(runner.cb_sink().clone())).expect("Failed to send ");
            runner.set_user_data(instance_tx);
            runner.add_global_callback('q', |s| s.quit());
            runner.add_global_callback('c', clear_statistics);
            runner.add_global_callback('p', plot);
            runner.add_global_callback('s', single_plot);

            let mut columns = Vec::new();
            for column in column_names.iter() {
                let mut vertical = LinearLayout::vertical().child(
                TextView::new(*column).full_width().fixed_height(2));
                for row in row_names.iter()
                {
                    let name = row.to_string() + " " + *column;
                    let text = if *column == column_names[0] { *row } else { "" };
                    vertical.add_child(TextView::new(text).with_name(name).full_width().min_height(1));
                }
                columns.push(vertical);
            }
            let mut table = LinearLayout::horizontal();
            for vertical in columns
            {
                table.add_child(vertical);
            }


            let mut medians = cursive::views::EditView::new().on_submit(set_medians);
            medians.set_content("1");
            let mut averages = cursive::views::EditView::new().on_submit(set_averages);
            averages.set_content("1");
            let mut window = cursive::views::EditView::new().on_submit(set_window);
            window.set_content("0");
            let controls = LinearLayout::horizontal()
            .child(TextView::new("Global Window"))
            .child(DummyView.fixed_width(1))
            .child(window.fixed_width(12))
            .child(DummyView.fixed_width(4))
            .child(TextView::new("Meidan window width"))
            .child(DummyView.fixed_width(1))
            .child(medians.fixed_width(12))
            .child(DummyView.fixed_width(4))
            .child(TextView::new("Averages window width"))
            .child(DummyView.fixed_width(1))
            .child(averages.fixed_width(12))
            .child(DummyView.fixed_width(4))
            .child(Button::new("(c)lear statistics", clear_statistics))
            .child(DummyView.fixed_width(4))
            .child(Button::new("(s)ingle plot", single_plot))
            .child(DummyView.fixed_width(4))
            .child(Button::new("(p)lot", plot))
            .child(DummyView.fixed_width(4))
            .child(Button::new("(q)uit", |s| s.quit()));

            runner.add_layer(LinearLayout::vertical()
                .child(table.full_screen())
                .child(controls));

            let mut frame_ts = Instant::now();
            while runner.is_running() {
                match instance_rx.try_recv() {
                    Err(std::sync::mpsc::TryRecvError::Disconnected) => runner.quit(),
                    Ok(msg) => {
                        let UiRequest::SetData(name, value) = msg;
                        runner.call_on_name(name.as_str(), |s: &mut TextView| s.set_content(value));
                    },
                    Err(std::sync::mpsc::TryRecvError::Empty) => {
                        if frame_ts.elapsed() > Duration::from_millis(100)
                        {
                            runner.step();
                            runner.refresh();
                            frame_ts = Instant::now();
                        }
                        else
                        {
                            std::thread::sleep(Duration::from_millis(1));
                        }
                    },
                }
            }
        }).unwrap();

        let sink: CbSink;
        if let UiResponse::Sink(s) = interface_rx.recv()? {
            sink = s;
        }
        else
        {
            return Err(RecvError);
        }

        Ok(Self {
            tx: interface_tx,
            rx: interface_rx,
            sink: sink,
            serial: "/dev/ttyACM0".to_owned(),
            plot: false,
            clear: false,
            single_plot: false,
            averages_width: 1,
            median_width: 1,
            window: 0,
        })
    }

    pub fn process(&mut self) -> bool {
        loop {
            match self.rx.try_recv() {
                Err(std::sync::mpsc::TryRecvError::Disconnected) => return false,
                Err(std::sync::mpsc::TryRecvError::Empty) => return true,
                Ok(msg) => match msg {
                    UiResponse::Clear => self.clear = true,
                    UiResponse::Plot => self.plot = !self.plot,
                    UiResponse::Serial(s) => self.serial = s,
                    UiResponse::Sink(s) => self.sink = s,
                    UiResponse::SinglePlot => self.single_plot = true,
                    UiResponse::Medians(width) => self.median_width = width,
                    UiResponse::Averages(width) => self.averages_width = width,
                    UiResponse::Window(width) => self.window = width,
                },
            }
        }
    }

    pub fn set_field(&self, name: String, value: String) {
        self.tx.send(UiRequest::SetData(name, value)).unwrap();
    }

    pub fn select_serial_dialog(&mut self) -> String {
        self.serial.clear();
        self.sink.send(Box::new(|s: &mut Cursive| {
            let select = SelectView::<String>::new().on_submit(set_serial).with_name("select");
            let button = Button::new("Refresh", refresh_ports);
            s.add_layer(Dialog::around(
        LinearLayout::vertical()
                .child(button)
                .child(select))
            .title("Select Serial").with_name("selector_dialog"));
            refresh_ports(s);
        })).unwrap();
        while self.process() {
            if !self.serial.is_empty() {
                return self.serial.clone();
            } else {
                std::thread::sleep(Duration::from_millis(1));
            }
        }
        String::default()
    }
}

fn clear_statistics(s: &mut Cursive) {
    if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Clear).unwrap(); }
}

fn plot(s: &mut Cursive) {
    if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Plot).unwrap(); }
}

fn single_plot(s: &mut Cursive) {
    if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::SinglePlot).unwrap(); }
}

fn set_medians(s: &mut Cursive, str: &str) {
    let parse = str::parse::<usize>(str);
    if let Ok(mut width) = parse {
        if width == 0 {
            width = 1;
        }
        if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Medians(width)).unwrap(); }
    }
}

fn set_averages(s: &mut Cursive, str: &str) {
    let parse = str::parse::<usize>(str);
    if let Ok(mut width) = parse {
        if width == 0 {
            width = 1;
        }
        if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Averages(width)).unwrap(); }
    }
}

fn set_window(s: &mut Cursive, str: &str) {
    let parse = str::parse::<usize>(str);
    if let Ok(width) = parse {
        if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Window(width)).unwrap(); }
    }
}

fn set_serial(s: &mut Cursive, path: &str) {
    s.pop_layer();
    if let Some(d) = s.user_data::<Sender<UiResponse>>() { d.send(UiResponse::Serial(path.to_owned())).unwrap(); }
}

fn refresh_ports(s: &mut Cursive) {
    let ports = serialport::available_ports().expect("Can not access serial ports");
    s.call_on_name("select", |select: &mut SelectView<String>| {
            select.clear();
            select.add_all_str(ports.into_iter().map(|w| w.port_name));
        });
}
