#![deny(unsafe_code)]

// std
use std::{convert::TryInto, io::Read, time::{Duration, Instant}, collections::VecDeque};
use serialport::{SerialPort};
use ui::UserInterface;
use wheelbuf;

mod ui;
mod plot;

const DATA_TYPES:usize = 4;

// #[derive(Default)]
#[derive(Debug)]
struct ChargerData<T> {
    desc: VecDeque<T>,
    names: [&'static str; DATA_TYPES],
    values: [VecDeque<f64>; DATA_TYPES],
    min: [f64; DATA_TYPES],
    max: [f64; DATA_TYPES],
    median_buf: [wheelbuf::WheelBuf<Vec<f64>, f64>; DATA_TYPES],
    average_buf: [wheelbuf::WheelBuf<Vec<f64>, f64>; DATA_TYPES],
    window: usize,
}

impl<T> ChargerData<T> {
    pub fn new(names: [&'static str; DATA_TYPES]) -> Self {
        let mut median_vec = Vec::<wheelbuf::WheelBuf<Vec<f64>, f64>>::default();
        let mut average_vec = Vec::<wheelbuf::WheelBuf<Vec<f64>, f64>>::default();
        for _ in 0..DATA_TYPES {
            median_vec.push(wheelbuf::WheelBuf::new(vec![f64::default()]));
            average_vec.push(wheelbuf::WheelBuf::new(vec![f64::default()]));
        }
        Self {
            min: [f64::MAX; DATA_TYPES],
            max: [f64::MIN; DATA_TYPES],
            values: Default::default(),
            desc: Default::default(),
            median_buf: median_vec.try_into().unwrap(),
            average_buf: average_vec.try_into().unwrap(),
            names,
            window: 0,
        }
    }

    pub fn set_median_width(&mut self, width: usize) {
        self.median_buf.iter_mut().for_each(|buf| *buf = wheelbuf::WheelBuf::new(vec![f64::default(); width]));
    }

    pub fn set_average_width(&mut self, width: usize) {
        self.average_buf.iter_mut().for_each(|buf| *buf = wheelbuf::WheelBuf::new(vec![f64::default(); width]));
    }

    pub fn push(&mut self, desc: T, data: &[f64; DATA_TYPES]) {
        self.desc.push_back(desc);
        let post_median = self.filter_median(data);
        let post_average = self.filter_average(&post_median);
        for i in 0..DATA_TYPES {
            self.values[i].push_back(post_average[i]);
            if self.max[i] < post_average[i] {
                self.max[i] = post_average[i];
            }
            if self.min[i] > post_average[i] {
                self.min[i] = post_average[i];
            }
        }
        while self.window != 0 && self.desc.len() > self.window {
            self.desc.pop_front();
            self.values.iter_mut().for_each(|f| drop(f.pop_front()));
        }
    }

    pub fn get_desc(&self) -> &VecDeque<T> {
        &self.desc
    }

    pub fn get_data(&self, element: usize) -> &VecDeque<f64> {
        &self.values[element]
    }

    pub fn get_name(&self, element: usize) -> &'static str {
        self.names[element]
    }

    pub fn update(&mut self, ui: &UserInterface, elapsed: Duration) {
        ui.set_field("Elapsed Value".to_owned(), format!("{:.1}", elapsed.as_secs_f32()));
        ui.set_field("Samples Value".to_owned(), self.desc.len().to_string());
        if self.desc.is_empty() {
            return;
        }
        for i in 0..DATA_TYPES {
            let sum: f64 = self.values[i].iter().map(|a| *a).sum();
            ui.set_field(self.names[i].to_owned() + " Value", format!("{:.3}", self.values[i].iter().last().unwrap()));
            ui.set_field(self.names[i].to_owned() + " Average", format!("{:.3}", (sum / self.values[i].len() as f64)));
            if self.window != 0 {
                self.min[i] = *self.values[i].iter().min_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap_or(&0.0);
                self.max[i] = *self.values[i].iter().max_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap_or(&0.0);
            }
            ui.set_field(self.names[i].to_owned() + " Min", format!("{:.3}", self.min[i]));
            ui.set_field(self.names[i].to_owned() + " Max", format!("{:.3}", self.max[i]));
            ui.set_field(self.names[i].to_owned() + " PtP", format!("{:.3}", (self.max[i] - self.min[i])));
        }
    }

    pub fn set_window(&mut self, width: usize) {
        if width != self.window {
            self.window = width;
            self.reset_statistics();
        }
    }

    pub fn reset_statistics(&mut self) {
        if self.window != 0 {
            self.values = Default::default();
            self.desc = Default::default();
        }
        self.min.iter_mut().for_each(|min| *min = f64::MAX);
        self.max.iter_mut().for_each(|max| *max = f64::MIN);
    }

    fn filter_average(&mut self, data: &[f64; DATA_TYPES]) -> [f64; DATA_TYPES] {
        let vec: Vec<f64> = self.average_buf.iter_mut().zip(data).map(|(buf, &new_val)| {
            buf.push(new_val);
            let sum:f64 = buf.iter().map(|a| *a).sum();
            sum / buf.len() as f64
        }).collect();
        vec.try_into().unwrap()
    }

    fn filter_median(&mut self, data: &[f64; DATA_TYPES]) -> [f64; DATA_TYPES] {
        let vec: Vec<f64> = self.median_buf.iter_mut().zip(data).map(|(buf, new_val)| {
            buf.push(*new_val);
            let mut x:Vec<f64> = buf.iter().map(|a| *a).collect();
            x.sort_by(|a, b| a.partial_cmp(b).unwrap());
            match x.get(x.len() / 2 + 1) {
                None => return *x.get(x.len() / 2).unwrap(),
                Some(x) => return *x,
            }
        }).collect();
        vec.try_into().unwrap()
    }
}


fn main() {
    const ROWS: [&str; 6] = ["Samples", "Elapsed", "Vin", "5V", "Vbat", "Ibat"];
    const COLUMNS: [&str; 6] = ["Field", "Value", "Average", "Min", "Max", "PtP"];

    let mut ui = ui::UserInterface::new(&ROWS, &COLUMNS).expect("Filed to initialize UI");
    let plot = plot::Plot::new();
    let mut serial: Option<Box<dyn SerialPort>> = None;

    let mut data = ChargerData::new(ROWS[2..].try_into().unwrap());
    data.set_median_width(5);
    let start_ts = Instant::now();
    let mut plot_update_ts = start_ts;
    let mut data_update_ts = start_ts;
    while ui.process() {
        if let Some(port) = &mut serial
        {
            let btr = port.bytes_to_read();
            if btr.is_ok()
            {
                let mut len = btr.unwrap();
                if len == 0
                {
                    std::thread::sleep(Duration::from_millis(1));
                }
                while len >= 10
                {
                    // Print data
                    let mut received_data = [0u8; 8];
                    let read_res = port.read_exact(&mut received_data);
                    let sync_res = synchronize(port);
                    if  read_res.is_ok() && sync_res.is_ok()
                    {
                        if sync_res.unwrap()
                        {
                            let mut values = [0u16; 4];
                            for (i, bytes) in received_data.chunks_exact(2).enumerate()
                            {
                                values[i] = u16::from_le_bytes((*bytes).try_into().unwrap());
                            }
                            let mut calculated = [
                                values[0] as f64 * 3.3 * 6.6 / 4096.0,
                                values[1] as f64 * 3.3 * 6.6 / 4096.0,
                                values[2] as f64 * 3.3 * 2.0 / 4096.0,
                                values[3] as f64 * 3.3 * 2.0 / 4096.0,
                            ];
                            calculated[3] = calculated[2] / 2.0 - calculated[3];
                            data.push(start_ts.elapsed().as_secs_f64(), &calculated);
                            len -= 10;
                        } else {
                            break;
                        }
                    }
                    else
                    {
                        serial = None;
                        break;
                    }
                }
            }
            else
            {
                serial = None;
            }
        }
        else
        {
            let path = ui.select_serial_dialog();
            serial = open_port(&path).ok();
            if let Some(port) = &mut serial
            {
                synchronize(port).ok();
            }
        }

        if ui.window != data.window {
            data.set_window(ui.window);
        }
        
        if ui.median_width != data.median_buf[0].len() {
            data.set_median_width(ui.median_width)
        }

        if ui.averages_width != data.average_buf[0].len() {
            data.set_average_width(ui.averages_width)
        }

        if data_update_ts.elapsed() > Duration::from_millis(30) {
            if ui.clear {
                ui.clear = false;
                data.reset_statistics();
            }
            data.update(&ui, start_ts.elapsed());
            data_update_ts = Instant::now();
        }

        if (ui.plot || ui.single_plot) && !data.get_desc().is_empty()
            && (plot_update_ts.elapsed() > Duration::from_millis(1000))
        {
            ui.single_plot = false;
            plot_update_ts = Instant::now();
            if plot_data(&plot, &data).is_err() {
                ui.plot = false;
            }
        }
    }
}

fn plot_data(plot: &plot::Plot, data: &ChargerData<f64>) -> Result<(),()>{
    plot.set_x(data.get_desc())?;

    for i in 0..DATA_TYPES {
        plot.add_signal(data.get_data(i) , data.get_name(i))?;
    }
    plot.show()
}

fn synchronize(reader: &mut Box<dyn SerialPort>) -> Result<bool, std::io::Error>
{
    const HEADER: u16 = 0xFFFF;
    let mut header;
    loop {
        let mut head_buf = [0u8; 2];
        match reader.read_exact(&mut head_buf) {
            Ok(()) => {
                header = u16::from_le_bytes(head_buf);
                if header == HEADER
                {
                    return Ok(true);
                }
                break;
            },
            Err(e) => {
                if e.kind() != std::io::ErrorKind::TimedOut
                {
                    return Err(e);
                }
            },
        }
    };

    // Synchronize header
    while header != 0xFFFF {
        let mut byte = [0u8];
        match reader.read_exact(&mut byte)
        {
            Ok(()) => (),
            Err(e) => {
                if e.kind() == std::io::ErrorKind::TimedOut
                {
                    continue;
                }
                return Err(e);
            }
        }
        header = ((header << 8) & 0xFF00) | byte[0] as u16;
    }
    Ok(false)
}


fn open_port(path: &str) -> Result<Box<dyn SerialPort>, std::io::Error>
{
    let serial = serialport::new(path, 1_500_000).open()?;
    std::thread::sleep(Duration::from_millis(10));
    serial.clear(serialport::ClearBuffer::All)?;
    Ok(serial)
}
