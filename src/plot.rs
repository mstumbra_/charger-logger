use std::collections::VecDeque;

use gnuplot::*;

enum Command {
    SetX(VecDeque<f64>),
    AddY(VecDeque<f64>, &'static str),
    Show
}

pub struct Plot {
    tx: std::sync::mpsc::Sender<Command>,
}

impl Plot {
    pub fn new() -> Self {
        let (tx, rx) = std::sync::mpsc::channel();
        std::thread::Builder::new().name("gnuplot".to_owned()).spawn(move ||{
            let mut fg = Figure::new();
            let mut axes = fg.axes2d();
            let mut x = VecDeque::new();
            while let Ok(cmd) = rx.recv() {
                match cmd {
                    Command::Show => { drop(axes); fg.show_and_keep_running().unwrap(); axes = fg.axes2d(); },
                    Command::SetX(new_x) => { fg.clear_axes(); axes = fg.axes2d(); x = new_x; },
                    Command::AddY(y, name) => { axes.lines(&x, y, &[PlotOption::Caption(name)]); },
                }
            }
        }).unwrap();
        Plot {tx}
    }

    pub fn set_x(&self, data: &VecDeque<f64>) -> Result<(),()> {
        if self.tx.send(Command::SetX(data.clone())).is_ok()
            { Ok(()) } else { Err(()) }
    }

    pub fn add_signal(&self, data: &VecDeque<f64>, name: &'static str) -> Result<(),()> {
        if self.tx.send(Command::AddY(data.clone(), name)).is_ok()
        { Ok(()) } else { Err(()) }
    }

    pub fn show(&self) -> Result<(),()>{
        if self.tx.send(Command::Show).is_ok()
            { Ok(()) } else { Err(()) }
    }
}
